package com.example.bandanar;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.bandanar.adapter.CategoryAdapter;
import com.example.bandanar.adapter.DiscountedProductAdapter;
import com.example.bandanar.adapter.RecentlyViewedAdapter;
import com.example.bandanar.model.Category;
import com.example.bandanar.model.DiscountedProducts;
import com.example.bandanar.model.RecentlyViewed;

import java.util.ArrayList;
import java.util.List;

import static com.example.bandanar.R.drawable.*;
import static com.example.bandanar.R.drawable.b1;
import static com.example.bandanar.R.drawable.b2;
import static com.example.bandanar.R.drawable.b3;
import static com.example.bandanar.R.drawable.b4;
import static com.example.bandanar.R.drawable.bandanas;
import static com.example.bandanar.R.drawable.card1;
import static com.example.bandanar.R.drawable.card2;
import static com.example.bandanar.R.drawable.card3;
import static com.example.bandanar.R.drawable.card4;
import static com.example.bandanar.R.drawable.cowboyhats;
import static com.example.bandanar.R.drawable.discountbandanas;
import static com.example.bandanar.R.drawable.discountglasses;
import static com.example.bandanar.R.drawable.discounthats;
import static com.example.bandanar.R.drawable.glasses;
import static com.example.bandanar.R.drawable.masks;

public class MainActivity extends AppCompatActivity {

    RecyclerView discountRecyclerView, categoryRecyclerView, recentlyViewedRecycler;
    DiscountedProductAdapter discountedProductAdapter;
    List<DiscountedProducts> discountedProductsList;

    CategoryAdapter categoryAdapter;
    List<Category> categoryList;

    RecentlyViewedAdapter recentlyViewedAdapter;
    List<RecentlyViewed> recentlyViewedList;

    TextView allCategory;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        discountRecyclerView = findViewById(R.id.discountedRecycler);
        categoryRecyclerView = findViewById(R.id.categoryRecycler);
        allCategory = findViewById(R.id.allCategoryImage);
        recentlyViewedRecycler = findViewById(R.id.recently_item);


        allCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this, AllCategory.class);
                startActivity(i);
            }
        });

        // adding data to model
        discountedProductsList = new ArrayList<>();
        discountedProductsList.add(new DiscountedProducts(1, discountbandanas));
        discountedProductsList.add(new DiscountedProducts(2, discounthats));
        discountedProductsList.add(new DiscountedProducts(3, discountglasses));
        discountedProductsList.add(new DiscountedProducts(4, discountbandanas));

        // adding data to model
        categoryList = new ArrayList<>();
        categoryList.add(new Category(1, bandanas));
        categoryList.add(new Category(2, cowboyhats));
        categoryList.add(new Category(3, masks));
        categoryList.add(new Category(4, glasses));

        // adding data to model
        recentlyViewedList = new ArrayList<>();
        recentlyViewedList.add(new RecentlyViewed("Bandana scarf, SILVER", "Large coloured handkerchief, typically with white spots, worn tied around the head or neck.", "$ 25", card1, b1));
        recentlyViewedList.add(new RecentlyViewed("Bandana scarf, BLACK", "Large coloured handkerchief, typically with white spots, worn tied around the head or neck.", "$ 30", card2, b2));
        recentlyViewedList.add(new RecentlyViewed("kiddow , GREEN", "Knit cap, originally of wool (though now often of synthetic fibers), is designed to provide warmth in cold weather. Typically, the knit cap is of simple, tapering constructions, though many variants exist.", "$ 25", card3, b3));
        recentlyViewedList.add(new RecentlyViewed("kiddow , BLACK", "Knit cap, originally of wool (though now often of synthetic fibers), is designed to provide warmth in cold weather. Typically, the knit cap is of simple, tapering constructions, though many variants exist.", "$ 20", card4, b4));

        setDiscountedRecycler(discountedProductsList);
        setCategoryRecycler(categoryList);
        setRecentlyViewedRecycler(recentlyViewedList);

    }

    private void setDiscountedRecycler(List<DiscountedProducts> dataList) {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        discountRecyclerView.setLayoutManager(layoutManager);
        discountedProductAdapter = new DiscountedProductAdapter(this, dataList);
        discountRecyclerView.setAdapter(discountedProductAdapter);
    }


    private void setCategoryRecycler(List<Category> categoryDataList) {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        categoryRecyclerView.setLayoutManager(layoutManager);
        categoryAdapter = new CategoryAdapter(this, categoryDataList);
        categoryRecyclerView.setAdapter(categoryAdapter);
    }

    private void setRecentlyViewedRecycler(List<RecentlyViewed> recentlyViewedDataList) {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        recentlyViewedRecycler.setLayoutManager(layoutManager);
        recentlyViewedAdapter = new RecentlyViewedAdapter(this, recentlyViewedDataList);
        recentlyViewedRecycler.setAdapter(recentlyViewedAdapter);
    }
}
